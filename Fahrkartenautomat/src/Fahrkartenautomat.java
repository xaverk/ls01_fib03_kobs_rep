﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {

		while (true) {

			Scanner tastatur = new Scanner(System.in);

			double zuZahlenderBetrag;
			double rückgabebetrag;

			/*
			 * double eingezahlterGesamtbetrag; double eingeworfeneMünze; int ticketanzahl =
			 * 0;
			 */

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------

			rueckgeldAusgeben(rückgabebetrag);
		}
	}

	static double fahrkartenbestellungErfassen() {

			
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag = 0;
		int ticketanzahl = 0;
		int ticketwahl;
		boolean bezahlen = false;
		double preis = 0;

		String[] FahrkartenBezeichnung= new String[10];
		
		FahrkartenBezeichnung[0] = "Einzelfahrschein AB";
		FahrkartenBezeichnung[1] = "Einzelfahrschein BC";
		FahrkartenBezeichnung[2] = "Einzelfahrschein ABC";
		FahrkartenBezeichnung[3] = "Kurzstrecke";
		FahrkartenBezeichnung[4] = "Tageskarte AB"; 
		FahrkartenBezeichnung[5] = "Tageskarte BC";
		FahrkartenBezeichnung[6] = "Tageskarte ABC";
		FahrkartenBezeichnung[7] = "Kleingruppe-Tageskarte AB";
		FahrkartenBezeichnung[8] = "Kleingruppe-Tageskarte BC";
		FahrkartenBezeichnung[9] = "Kleingruppe-Tageskarte ABC";
		
		double[] FahrkartenPreis= new double[10];
		
		FahrkartenPreis[0] = 2.90;
		FahrkartenPreis[1] = 3.30;
		FahrkartenPreis[2] = 3.60;
		FahrkartenPreis[3] = 1.90;
		FahrkartenPreis[4] = 8.60;
		FahrkartenPreis[5] = 9.00;
		FahrkartenPreis[6] = 9.60;
		FahrkartenPreis[7] = 23.50;
		FahrkartenPreis[8] = 24.30;
		FahrkartenPreis[9] = 24.90;
		
		
		while (bezahlen == false) {

			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin aus:\n");
			
			for(int i = 0; i < FahrkartenBezeichnung.length; i++) {
			
			int j = i+1;
			System.out.println(FahrkartenBezeichnung[i] + " für " + FahrkartenPreis[i] + " EUR " + "(" + j + ")");
			}
			
			System.out.print("\nBezahlen (11)\n");
			

		ticketwahl = tastatur.nextInt();

		
		
		switch (ticketwahl) {
		case 1:
			preis = FahrkartenPreis[0];
			System.out.println(FahrkartenBezeichnung[0] + " für " + FahrkartenPreis[0] + " EUR");
			break;
			
		case 2:
			preis = FahrkartenPreis[1];
			System.out.println(FahrkartenBezeichnung[1] + " für " + FahrkartenPreis[1] + " EUR");
			break;

		case 3:
			preis = FahrkartenPreis[2];
			System.out.println(FahrkartenBezeichnung[2] + " für " + FahrkartenPreis[2] + " EUR");
			break;
			
		case 4:
			preis = FahrkartenPreis[3];
			System.out.println(FahrkartenBezeichnung[3] + " für " + FahrkartenPreis[3] + " EUR");
			break;
			
		case 5:
			preis = FahrkartenPreis[4];
			System.out.println(FahrkartenBezeichnung[4] + " für " + FahrkartenPreis[4] + " EUR");
			break;
			
		case 6:
			preis = FahrkartenPreis[5];
			System.out.println(FahrkartenBezeichnung[5] + " für " + FahrkartenPreis[5] + " EUR");
			break;
			
		case 7:
			preis = FahrkartenPreis[6];
			System.out.println(FahrkartenBezeichnung[6] + " für " + FahrkartenPreis[6] + " EUR");
			break;
			
		case 8:
			preis = FahrkartenPreis[7];
			System.out.println(FahrkartenBezeichnung[7] + " für " + FahrkartenPreis[7] + " EUR");
			break;
			
		case 9:
			preis = FahrkartenPreis[8];
			System.out.println(FahrkartenBezeichnung[8] + " für " + FahrkartenPreis[8] + " EUR");
			break;
			
		case 10:
			preis = FahrkartenPreis[9];
			System.out.println(FahrkartenBezeichnung[9] + " für " + FahrkartenPreis[9] + " EUR");
			break;
			
			
		case 11:
			bezahlen = true;
			break;

		default:
			System.out.println("Es können nur 1,2,3,5,6,7,8,9,10 oder 11 Ausgewählt werden.]");
			continue;
		}
		
		if (bezahlen == false) {
			
		
		System.out.println("Wie viele Tickets?: ");
		ticketanzahl = tastatur.nextInt();

		while (ticketanzahl > 10 || ticketanzahl < 1) {
			System.out.println("Fehler, es können nur 1-10 Tickets gekauft werden! Geben Sie eine gültige Zahl ein");
			ticketanzahl = tastatur.nextInt();
		}
		
		zuZahlenderBetrag = zuZahlenderBetrag + (preis * ticketanzahl);
		}
		}
		return zuZahlenderBetrag;

	}

	static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		double rückgabebetrag;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;

	}

	static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);

		}
		System.out.println("\n\n");

	}

	static void rueckgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "Euro");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "Euro");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "Cent");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "Cent");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "Cent");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "Cent");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");

	}

	static void warte(int millisekunde) {
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

}